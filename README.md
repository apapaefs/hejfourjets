# hejfourjets



## Description

The goal is to use the HEJ event generator (https://hej.hepforge.org/) to describe results of the article "Measurement of double-parton scattering in inclusive production of four jets with low transverse momentum in proton-proton collisions at √s = 13 TeV" (https://arxiv.org/pdf/2109.13822.pdf).
